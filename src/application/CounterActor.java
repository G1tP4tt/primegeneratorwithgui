package application;

import akka.actor.UntypedActor;

public class CounterActor extends UntypedActor {

	public int lowerBorder;
	public int upperBorder;

	public CounterActor(Boundaries boundaries) {
		this.lowerBorder = boundaries.lower;
		this.upperBorder = boundaries.upper;
	}

	public CounterActor() {

	}

	public CounterActor(int lower, int upper) {

		this.lowerBorder = lower;
		this.upperBorder = upper;
	}

	@Override
	public void onReceive(Object obj) throws Exception {

		if (obj.getClass() == Boundaries.class) {
			Boundaries b = (Boundaries) obj;
			this.lowerBorder = b.lower;
			this.upperBorder = b.upper;

		} else {
			Counter c = (Counter) obj;
			int primesInRange = countPrimesInRange(lowerBorder, upperBorder);

			getContext().replySafe(primesInRange);
			c.updateProgress();

		}
	}

	protected int countPrimesInRange(int lower, int upper) {

		int primeCount = 0;
		for (int i = lower; i <= upper; i++) {
			if (isPrime(i)) {
				primeCount++;
			}
		}

		return primeCount;
	}

	boolean isPrime(int n) {
		// check if n is a multiple of 2
		if (n % 2 == 0)
			return false;
		// if not, then just check the odds
		for (int i = 3; i * i <= n; i += 2) {
			if (n % i == 0)
				return false;
		}
		return true;
	}

}
