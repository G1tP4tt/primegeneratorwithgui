package application;

import java.util.concurrent.ExecutionException;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.control.Slider;

public class SampleController {
	Counter counter;
	long start;
	long end;
	Service<Integer> service;

	public SampleController() {

	}

	@FXML
	Button calcBtn;
	@FXML
	TextField upperIn;
	@FXML
	TextField lowerIn;
	@FXML
	TextField rangeIn;
	@FXML
	TextField resultOut;
	@FXML
	ProgressBar pBar;
	@FXML
	Label timeLabel;
	@FXML
	Button cancel;

	@FXML
	Slider lowerSlider;
	@FXML
	Slider upperSlider;

	@FXML
	protected void onDragLower() {
		lowerIn.setText(Double.toString((lowerSlider.getValue())));
	}

	@FXML
	protected void onDragUpper() {
		upperIn.setText(Double.toString((upperSlider.getValue())));
	}

	@FXML
	protected void calc() throws InterruptedException, ExecutionException {

		counter = new Counter(Integer.parseInt(lowerIn.getText()), Integer.parseInt(upperIn.getText()),
				Integer.parseInt(rangeIn.getText()));

		counter.setController(this);

		setGui("Let's Calculate! ", false, "");

		service = new Service<Integer>() {

			@Override
			protected Task<Integer> createTask() {
				return new Task<Integer>() {

					@Override
					protected Integer call() throws Exception {

						int sum = counter.Calculate();

						return sum;
					}

				};
			}

		};

		service.setOnFailed(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent t) {

				end = System.nanoTime();
				double timeTook = ((end - start) / 1.09e9);
				timeTook = Math.round(timeTook * 1000) / 1000.0;
				setGui("Execution failed after :" + timeTook, true, "");
			}
		});

		service.setOnCancelled(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent t) {

				setGui("Calculation cancelled. ", true, "");
				pBar.setProgress(0.0);
			}

		});

		service.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent t) {
				end = System.nanoTime();

				double timeTook = ((end - start) / 1.09e9);
				timeTook = Math.round(timeTook * 1000) / 1000.0;

				setGui("Sucessfully received primes!\nCalculation took : " + timeTook + " seconds.", true,
						t.getSource().getValue().toString());
			}
		});

		start = System.nanoTime();

		service.start();

	}

	private void setGui(String time, boolean showButton, String out) {
		timeLabel.setText(time);

		if (showButton) {
			pBar.setProgress(0.0);
			calcBtn.setVisible(true);

		} else {
			calcBtn.setVisible(false);
		}

		resultOut.setText(out);
	}

	public void updateBar(double i) {

		double progress = pBar.getProgress();
		progress = progress + (i / 100);
		pBar.setProgress(progress);

	}

}
