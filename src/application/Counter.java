package application;

import java.util.ArrayList;
import java.util.List;

import akka.actor.Actor;
import akka.actor.ActorRef;
import akka.actor.Actors;
import akka.actor.UntypedActorFactory;
import akka.dispatch.Future;

public class Counter {

	private int initLower;
	private int initUpper;
	private int rangePerPartition;
	private int sum;
	private int numberOfActors;
	private SampleController controller;

	public Counter(int lower, int upper, int partitions) {
		this.initLower = lower;
		this.initUpper = upper;
		this.rangePerPartition = (upper - lower) / partitions;
		this.numberOfActors = partitions;
	}

	public Counter() {

	}

	public int Calculate() {

		List<Future<Object>> results = new ArrayList<>(numberOfActors);

		int upper = rangePerPartition;

		ActorRef[] aktoren = new ActorRef[numberOfActors];

		int lower = this.initLower;

		int i = 0;

		while (i < numberOfActors - 1) {

			aktoren[i] = Actors.actorOf(new UntypedActorFactory() {
				@Override
				public Actor create() {
					return new CounterActor();
				}
			}).start();

			aktoren[i].sendOneWay(new Boundaries(lower, upper));

			results.add(aktoren[i].sendRequestReplyFuture(this));

			lower = upper + 1;
			upper += rangePerPartition;

			i++;
		}

		aktoren[i] = Actors.actorOf(new UntypedActorFactory() {
			@Override
			public Actor create() {
				return new CounterActor();
			}
		}).start();

		upper = initUpper;

		aktoren[i].sendOneWay(new Boundaries(lower, upper));
		results.add(aktoren[i].sendRequestReplyFuture(this));

		for (int j = 0; j < numberOfActors; j++) {

			sum += (int) results.get(j).await().get();

		}
		return sum;
	}

	public void updateProgress() {

		controller.updateBar(100.0 / numberOfActors);

	}

	public int getSum() {
		return sum;
	}

	public int getLower() {
		return initLower;
	}

	public void setLower(int initLower) {
		this.initLower = initLower;
	}

	public int getUpper() {
		return initUpper;
	}

	public void setUpper(int upper) {
		this.initUpper = upper;
	}

	public void setRange(int range) {
		this.rangePerPartition = range;
	}

	public void setController(SampleController sampleController) {
		this.controller = sampleController;

	}

}